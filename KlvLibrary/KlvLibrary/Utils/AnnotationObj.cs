﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace KlvLibrary.Utils
{
    public class AnnotationObj
    {
        public List<Annotation> datas { get; set; }

        //public AnnotationObj(Dictionary<string, string> telemetry)
        //{
        //    this.datas = new List<Annotation>();
        //    this.GenerateList(telemetry);
        //}

        public AnnotationObj(List<Annotation> telemetry)
        {
            this.datas = telemetry;
        }

        private void GenerateList(Dictionary<string, string> telemetry)
        {
            foreach (KeyValuePair<string, string> pair in telemetry)
            {
                datas.Add(new Annotation(pair.Key, pair.Value));
                //Console.WriteLine($"Key = {pair.Key}, Value = {pair.Value}");
            }
        }

        public override string ToString()
        {
            var options = new JsonSerializerOptions()
            {
                WriteIndented = true,
                Encoder = System.Text.Encodings.Web.JavaScriptEncoder.Create(System.Text.Unicode.UnicodeRanges.All)
            };
            return JsonSerializer.Serialize<AnnotationObj>(this, options);
        }
    }
}
