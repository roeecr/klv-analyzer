﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KlvLibrary.Utils
{
    public class KlvEventArg: EventArgs
    {
        private string obj;

        public string Obj { get => obj; set => obj = value; }

        public KlvEventArg(string obj)
        {
            this.obj = obj;
        }
    }
}
