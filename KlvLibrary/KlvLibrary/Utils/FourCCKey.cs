﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KlvLibrary.Utils
{

    
    class FourCCKey
    {
		public static UInt32 GPMF_KEY_DEVICE = Common.MakeKey("DEVC"); //DEVC - nested device data to speed the parsing of multiple devices in post 
		public static UInt32 GPMF_KEY_DEVICE_ID = Common.MakeKey("DVID");//DVID - unique id per stream for a metadata source (in camera or external input) (single 4 byte int)
		public static UInt32 GPMF_KEY_DEVICE_NAME = Common.MakeKey("DVNM");//DVNM - human readable device type/name (char string)
		public static UInt32 GPMF_KEY_STREAM = Common.MakeKey("STRM");//STRM - nested channel/stream of telemetry data
		public static UInt32 GPMF_KEY_STREAM_NAME = Common.MakeKey("STNM");//STNM - human readable telemetry/metadata stream type/name (char string)
		public static UInt32 GPMF_KEY_SI_UNITS = Common.MakeKey("SIUN");//SIUN - Display string for metadata units where inputs are in SI units "uT","rad/s","km/s","m/s","mm/s" etc.
		public static UInt32 GPMF_KEY_UNITS = Common.MakeKey("UNIT");//UNIT - Freedform display string for metadata units (char sting like "RPM", "MPH", "km/h", etc)
		public static UInt32 GPMF_KEY_MATRIX = Common.MakeKey("MTRX");//MTRX - 2D matrix for any sensor calibration.
		public static UInt32 GPMF_KEY_ORIENTATION_IN = Common.MakeKey("ORIN");//ORIN - input 'n' channel data orientation, lowercase is negative, e.g. "Zxy" or "ABGR".
		public static UInt32 GPMF_KEY_ORIENTATION_OUT = Common.MakeKey("ORIO");//ORIO - output 'n' channel data orientation, e.g. "XYZ" or "RGBA".
		public static UInt32 GPMF_KEY_SCALE = Common.MakeKey("SCAL");//SCAL - divisor for input data to scale to the correct units.
		public static UInt32 GPMF_KEY_TYPE = Common.MakeKey("TYPE");//TYPE - Type define for complex data structures
		public readonly UInt32 GPMF_KEY_TOTAL_SAMPLES = Common.MakeKey("TSMP");//TOTL - Total Sample Count including the current payload 	
		public readonly UInt32 GPMF_KEY_TICK = Common.MakeKey("TICK");//TICK - Beginning of data timing (arrival) in milliseconds. 
		public readonly UInt32 GPMF_KEY_TOCK = Common.MakeKey("TOCK");//TOCK - End of data timing (arrival)  in milliseconds. 
		public readonly UInt32 GPMF_KEY_TIME_OFFSET = Common.MakeKey("TIMO");//TIMO - Time offset of the metadata stream that follows (single 4 byte float)
		public readonly UInt32 GPMF_KEY_TIMING_OFFSET = Common.MakeKey("TIMO");//TIMO - duplicated, as older code might use the other version of TIMO
		public readonly UInt32 GPMF_KEY_TIME_STAMP = Common.MakeKey("STMP");//STMP - Time stamp for the first sample. 
		public readonly UInt32 GPMF_KEY_TIME_STAMPS = Common.MakeKey("STPS");//STPS - Stream of all the timestamps delivered (Generally don't use this. This would be if your sensor has no peroidic times, yet precision is required, or for debugging.) 
		public readonly UInt32 GPMF_KEY_PREFORMATTED = Common.MakeKey("PFRM");//PFRM - GPMF data
		public readonly UInt32 GPMF_KEY_TEMPERATURE_C = Common.MakeKey("TMPC");//TMPC - Temperature in Celsius
		public readonly UInt32 GPMF_KEY_EMPTY_PAYLOADS = Common.MakeKey("EMPT");//EMPT - Payloads that are empty since the device start (e.g. BLE disconnect.)
		public readonly UInt32 GPMF_KEY_QUANTIZE = Common.MakeKey("QUAN");//QUAN - quantize used to enable stream compression - 1 -  enable, 2+ enable and quantize by this value
		public readonly UInt32 GPMF_KEY_VERSION = Common.MakeKey("VERS");//VERS - version of the metadata stream (debugging)
		public readonly UInt32 GPMF_KEY_FREESPACE = Common.MakeKey("FREE");//FREE - n bytes reserved for more metadata added to an existing stream
		public readonly UInt32 GPMF_KEY_REMARK = Common.MakeKey("RMRK");//RMRK - adding comments to the bitstream (debugging)

		public static UInt32 GPMF_KEY_END = 0; //(null)

		private static readonly FourCCKey instance = new FourCCKey();

		static FourCCKey() { }

		private FourCCKey() { }

		public static FourCCKey getInstance()
		{
			return instance;
		}

	}
}
