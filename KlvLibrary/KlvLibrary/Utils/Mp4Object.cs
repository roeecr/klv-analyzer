﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace KlvLibrary.Utils
{
    public class Mp4Object
    {
        public UInt32[] metasizes;
        public UInt32 metasize_count;
        public UInt32[] metaoffsets;
        public UInt32 metastco_count;
        public UInt32 indexcount;
        public double videolength;
        public double metadatalength;
        public double fps;
        public UInt32 trak_clockdemon, trak_clockcount;
        public UInt32 meta_clockdemon, meta_clockcount;
        public UInt32 video_framerate_numerator;
        public UInt32 video_framerate_denominator;
        public UInt32 video_frames;
        public double basemetadataduration;
        public UInt32 trak_num;
        public DateTime date;
        public FileStream fs;

        public Mp4Object(string path)
        {
            this.fs = File.Open(path, FileMode.Open, FileAccess.ReadWrite);
        }

        
        #region Mp4Object Access Functions

        public UInt32 GetNumberPayloads()
        {
            return indexcount;
        }

        public byte[] GetPayload(int index)
        {
            if (index < indexcount && fs != null)
            {
                if ((UInt64)fs.Length >= metaoffsets[index] + metasizes[index] && metasizes[index] > 0)
                {
                    fs.Seek((UInt32)metaoffsets[index], SeekOrigin.Begin);
                    byte[] buffer = new byte[metasizes[index]];
                    fs.Read(buffer, 0, (int)metasizes[index]);

                    return buffer;
                }

                else
                {
                    Console.WriteLine("here1");
                }
            }

            Console.WriteLine("here2");
            return null;
        }

        public UInt32 GetPayloadSize(int index)
        {
            if (metasizes != null && metasize_count > index)
                return metasizes[index];

            return 0;
        }

        public float GetDuration()
        {
            if (this != null)
                return (float)metadatalength;

            return 0;
        }

        public UInt32 GetFramesNumber()
        {
            return video_frames;
        }

        public double GetFps()
        {
            return fps;
        }

        #endregion

        public override string ToString()
        {
            return $@"
                    ---------------------------------------------------------------
                    Mp4 Object be like:
                    metasize_count => {metasize_count}
                    metastco_count => {metastco_count}
                    indexcount => {indexcount}
                    videolength => {videolength}
                    metadatalength => {metadatalength}
                    fps => {fps}
                    trak_clockdemon => {trak_clockdemon}
                    trak_clockcount => {trak_clockcount}
                    meta_clockdemon => {meta_clockdemon}
                    meta_clockcount => {meta_clockcount}
                    video_framerate_numerator => {video_framerate_numerator}
                    video_framerate_denominator => {video_framerate_denominator}
                    video_frames => {video_frames}
                    basemetadataduration => {basemetadataduration}
                    trak_num => {trak_num}
                    date => {date.ToString()}
                    ---------------------------------------------------------------";
        }
    }
}
