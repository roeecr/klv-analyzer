﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace KlvLibrary.Utils
{
    public class KlvObj
    {
        public string deviceName { get; set; }
        public double timeOffset { get; set; }
        public double timeSpan { get; set; }
        public List<SensorData> datas { get; set; }

        public KlvObj(double timeSpan, string deviceName)
        {
            datas = new List<SensorData>();
            timeOffset = 0;
            this.timeSpan = timeSpan;
            this.deviceName = deviceName;
        }

        public int findKey(string key)
        {
            for (int i = 0; i < datas.Count; i++)
            {
                if (datas[i].key == key)
                    return i;
            }

            return -1;
        }

        public override string ToString()
        {
            var options = new JsonSerializerOptions()
            {
                WriteIndented = true,
                Encoder = System.Text.Encodings.Web.JavaScriptEncoder.Create(System.Text.Unicode.UnicodeRanges.All)
            };
            return JsonSerializer.Serialize<KlvObj>(this, options);
        }

        public List<SensorData> CopyList()
        {
            List<SensorData> lst = new List<SensorData>();
            for (int i = 0; i < this.datas.Count; i++)
            {
                lst.Add(this.datas[i]);
            }

            return lst;
        }

        public bool isEqualTo(List<SensorData> other)
        {
            if (other == null)
                return false;

            if (other.Count != this.datas.Count)
                return false;

            for(int i = 0; i < other.Count; i++)
            {
                SensorData sensor = other[i];
                if (!isSensorEqual(sensor))
                    return false;
            }

            return true;
        }

        public void RebuildObject(KlvObj other)
        {
            if (other == null)
                return;

            List<SensorData> datas = new List<SensorData>();
            for (int i = 0; i < this.datas.Count; i++)
            {
                SensorData sensor = this.datas[i];
                if (!other.isSensorEqual(sensor))
                    datas.Add(sensor);
            }

            this.datas = datas;
        }

        public KlvObj UpdateObject(KlvObj other)
        {
            if (other == null)
                return this;

            for (int i = 0; i < this.datas.Count; i++)
            {
                SensorData sensor = this.datas[i];
                int index = other.findKey(sensor.key);
                if (index != -1)
                    other.datas[index] = sensor;
            }

            return other;
        }

        public bool isSensorEqual(SensorData sensor)
        {
            int key = findKey(sensor.key);
            if (key == -1)
                return false;
            else
                return sensor.data == this.datas[key].data;
        }
    }
}
