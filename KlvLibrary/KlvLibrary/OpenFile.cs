﻿using System;
using System.IO;
using System.Text;
using KlvLibrary.Utils;

namespace KlvLibrary
{
    public class OpenFile
    {
        public static bool DEBUD = false;
        private Mp4Object mp4;
        private byte[] sizeb = new byte[4];
        private byte[] tagb = new byte[4];
        private byte[] skipb = new byte[4];
        private byte[] datab = new byte[4];
        private byte[] subtypeb = new byte[4];
        private int len = 0;
        private UInt32 data, size, tag, type = 0;
        private UInt32 traktype, traksubtype;
        private string tagStr;

        private DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, 0);

        public OpenFile()
        {

        }

        public Mp4Object openMP4(string path, UInt32 traktype, UInt32 traksubtype)
        {

            if (File.Exists(path))
            {
                this.mp4 = new Mp4Object(path);
                this.traksubtype = traksubtype;
                this.traktype = traktype;

                do
                {
                    len = mp4.fs.Read(sizeb, 0, 4);
                    len += mp4.fs.Read(tagb, 0, 4);

                    size = BitConverter.ToUInt32(sizeb);
                    tag = BitConverter.ToUInt32(tagb);


                    if (len == 8 && mp4.fs.Position < mp4.fs.Length)
                    {
                        if (mp4.fs.Position == 8 && tag != Common.MakeKey("ftyp"))
                        {
                            break;
                        }

                        size = Common.ByteSwap32(size);

                        if (IsUnnecessaryTag(tag))
                        {
                            LongSeek(mp4.fs, size - 8);
                        }
                        else
                        {
                            tagStr = Common.Key2FourCC(tag);

                            switch (tagStr)
                            {
                               
                                case "trak":
                                    DoTRAK();
                                    break;

                                case "mdhd":
                                    DoMDHD();
                                    break;

                                case "hdlr":
                                    DoHDLR();
                                    break;

                                case "stsd":
                                    DoSTSD();
                                    break;

                                case "stsz":
                                    DoSTSZ();
                                    break;

                                case "stco":
                                    DoSTCO();
                                    break;

                                case "stts":
                                    DoSTTS();
                                    break;


                                default:
                                    break;

                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                } while (len > 0);

                mp4.indexcount = mp4.metasize_count;
                if (DEBUD) Console.WriteLine("end of file");
                return mp4;
            }
            else
            {
                if (DEBUD) Console.WriteLine("not found");
            }
            return null;
        }

        #region UDTA

        public Mp4Object openUDTA(string path)
        {
            if (File.Exists(path))
            {
                Mp4Object mp4 = new Mp4Object(path);

                UInt64 lastsize = 0;
                int len = 0;
                byte[] sizeb = new byte[4];
                byte[] tagb = new byte[4];
                byte[] skipb = new byte[4];
                byte[] datab = new byte[4];
                byte[] subtypeb = new byte[4];
                UInt32 type = 0;

                if (DEBUD) Console.WriteLine("fs length is {0}", mp4.fs.Length);

                do
                {
                    len = mp4.fs.Read(sizeb, 0, 4);
                    len += mp4.fs.Read(tagb, 0, 4);

                    if (len == 8)
                    {
                        UInt32 tag = BitConverter.ToUInt32(tagb);
                        UInt32 data;
                        string key = Common.Key2FourCC(tag);

                        if (key == "GPMF" || key == "mdat" || key == "ftyp" || key == "moov" || key == "udta")
                        {
                            if (DEBUD) Console.WriteLine("here");
                        }
                    }
                } while (len > 0);
            }
            else
            {
                Console.WriteLine("not found");
            }
            return null;
        }

        public void LongSeekUDTA(FileStream fs, Int64 offest)
        {
            if (fs.Position + (UInt32)offest < fs.Length)
                fs.Seek(offest, SeekOrigin.Current);
            else
            {
                //fs.Seek(fs.Length, SeekOrigin.Begin);
            }

        }

        #endregion

        public void LongSeek(FileStream fs, UInt32 offest)
        {
            fs.Seek(offest, SeekOrigin.Current);
        }

        public bool IsUnnecessaryTag(UInt32 tag)
        {
            return  tag != Common.MakeKey("moov") && //skip over all but these atoms
                    tag != Common.MakeKey("trak") &&
                    tag != Common.MakeKey("mdia") &&
                    tag != Common.MakeKey("mdhd") &&
                    tag != Common.MakeKey("minf") &&
                    tag != Common.MakeKey("gmin") &&
                    tag != Common.MakeKey("dinf") &&
                    tag != Common.MakeKey("alis") &&
                    tag != Common.MakeKey("stsd") &&
                    tag != Common.MakeKey("stbl") &&
                    tag != Common.MakeKey("stts") &&
                    tag != Common.MakeKey("stsz") &&
                    tag != Common.MakeKey("stco") &&
                    tag != Common.MakeKey("hdlr");
        }

        #region Do Tag

        private void DoTRAK()
        {
            mp4.trak_num++;
            if (DEBUD) Console.WriteLine("trak {0}", mp4.trak_num);
        }

        private void DoMDHD()
        {
            if (DEBUD) Console.WriteLine("mdhd");
            len = mp4.fs.Read(skipb, 0, 4);
            len += mp4.fs.Read(datab, 0, 4);
            data = BitConverter.ToUInt32(datab);
            data = Common.ByteSwap32(data);
            if (DEBUD) Console.WriteLine("creation time is : {0}", data);

            if (DEBUD) Console.WriteLine("seconds {0}", data);
            dateTime = dateTime.AddSeconds(data / 10);
            mp4.date = dateTime;

            len += mp4.fs.Read(datab, 0, 4);
            data = BitConverter.ToUInt32(datab);
            data = Common.ByteSwap32(data);
            if (DEBUD) Console.WriteLine("modification time is : {0}", data);

            len += mp4.fs.Read(datab, 0, 4);
            data = BitConverter.ToUInt32(datab);
            mp4.trak_clockdemon = Common.ByteSwap32(data);
            if (DEBUD) Console.WriteLine("time scale is : {0:x}", mp4.trak_clockdemon);

            len += mp4.fs.Read(datab, 0, 4);
            data = BitConverter.ToUInt32(datab);
            mp4.trak_clockcount = Common.ByteSwap32(data);
            if (DEBUD) Console.WriteLine("duration is : {0}", mp4.trak_clockcount);

            if (mp4.videolength == 0.0)
            {
                mp4.videolength = (double)mp4.trak_clockcount / mp4.trak_clockdemon;
                if (DEBUD) Console.WriteLine("video length {0}", mp4.videolength);
            }


            LongSeek(mp4.fs, size - 8 - (UInt32)len);
        }

        private void DoHDLR()
        {
            len = mp4.fs.Read(skipb, 0, 4);
            len += mp4.fs.Read(skipb, 0, 4);
            len += mp4.fs.Read(datab, 0, 4);  // type will be 'meta' for the correct trak.

            UInt32 temp = BitConverter.ToUInt32(datab);
            if (DEBUD) Console.WriteLine("temp is {0}", temp);

            if (temp != Common.MakeKey("alis") && temp != Common.MakeKey("url "))
                type = temp;


            LongSeek(mp4.fs, size - 8 - (UInt32)len); // skip over hldr
        }

        private void DoSTSD()
        {
            if (DEBUD) Console.WriteLine("stsd");
            if (type == traktype) //like meta
            {
                len = mp4.fs.Read(skipb, 0, 4);
                len += mp4.fs.Read(skipb, 0, 4);
                len += mp4.fs.Read(skipb, 0, 4);
                len += mp4.fs.Read(subtypeb, 0, 4);  // type will be 'meta' for the correct trak.

                UInt32 subtype = BitConverter.ToUInt32(subtypeb);

                if (DEBUD) Console.WriteLine("subtype -> {0}", subtype);

                if (len == 16)
                {
                    if (subtype != traksubtype) // not MP4 metadata 
                    {
                        type = 0; // MP4
                    }
                }

                LongSeek(mp4.fs, size - 8 - (UInt32)len); // skip over stsd
            }
            else
                LongSeek(mp4.fs, size - 8);
        }

        private void DoSTSZ()
        {
            if (DEBUD) Console.WriteLine("stsz");

            if (type == traktype) // meta
            {
                UInt32 equalsamplesize, num;

                len = mp4.fs.Read(skipb, 0, 4);
                len += mp4.fs.Read(datab, 0, 4);
                equalsamplesize = BitConverter.ToUInt32(datab);

                len += mp4.fs.Read(datab, 0, 4);
                num = BitConverter.ToUInt32(datab);
                num = Common.ByteSwap32(num);

                if (DEBUD) Console.WriteLine("num is {0}\nequalsample is {1}", num, equalsamplesize);

                // if equalsamplesize != 0, it is the size of all the samples and the length should be 20 (size,fourcc,flags,samplesize,samplecount)
                if ((num <= (size / sizeof(UInt32))) || (equalsamplesize != 0 && size == 20))
                {
                    //either the samples are different sizes or they are all the same size
                    if (num > 0 && ((size > (num * 4)) || (equalsamplesize != 0 && size == 20)))
                    {
                        mp4.metasize_count = num;
                        mp4.metasizes = new UInt32[num * 4];

                        if (equalsamplesize == 0)
                        {
                            for (int i = 0; i < num; i++)
                            {
                                len += mp4.fs.Read(datab, 0, 4);
                                mp4.metasizes[i] = BitConverter.ToUInt32(datab);
                                mp4.metasizes[i] = Common.ByteSwap32(mp4.metasizes[i]);

                                if (DEBUD) Console.WriteLine("meta size of {0} is {1}", i, mp4.metasizes[i]);
                            }
                        }
                        else
                        {
                            equalsamplesize = Common.ByteSwap32(equalsamplesize);
                            do
                            {
                                num--;
                                mp4.metasizes[num] = equalsamplesize;

                                if (DEBUD) Console.WriteLine("meta size of {0} is {1}", num, mp4.metasizes[num]);
                            } while (num > 0);
                        }
                        if (DEBUD) Console.WriteLine("metasize_count: {0}", mp4.metasize_count);
                        LongSeek(mp4.fs, size - 8 - (UInt32)len); // skip over stsz
                    }
                    else
                    {
                        //size of null
                        if (DEBUD) Console.WriteLine("break out - size of null");
                    }
                }
                else
                    LongSeek(mp4.fs, size - 8 - (UInt32)len); // skip over stsz
            }
            else
                LongSeek(mp4.fs, size - 8);
        }

        private void DoSTCO()
        {
            if (DEBUD) Console.WriteLine("stco");
            if (type == traktype) // meta
            {
                len = mp4.fs.Read(skipb, 0, 4);
                len += mp4.fs.Read(datab, 0, 4);
                UInt32 num = BitConverter.ToUInt32(datab);
                num = Common.ByteSwap32(num);

                if (DEBUD) Console.WriteLine("num -> {0}", num);

                if (num <= ((size - 8 - len) / sizeof(UInt32)))
                {
                    mp4.metastco_count = num;
                    if (DEBUD) Console.WriteLine("mp4.metastco_count -> {0}", mp4.metastco_count);
                }

                mp4.metaoffsets = new UInt32[num];
                if (mp4.metaoffsets != null)
                {
                    for (int i = 0; i < num; i++)
                    {
                        int readlen = mp4.fs.Read(datab, 0, 4);
                        data = BitConverter.ToUInt32(datab);
                        mp4.metaoffsets[i] = Common.ByteSwap32(data);
                        if (DEBUD) Console.WriteLine("meta offsets of {0} is {1}", i, mp4.metaoffsets[i]);
                        len += readlen;
                    }

                }

                LongSeek(mp4.fs, size - 8 - (UInt32)len); // skip over stco
            }
            else
                LongSeek(mp4.fs, size - 8); // skip over stco
        }

        private void DoSTTS()
        {
            if (DEBUD) Console.WriteLine("stts");

            if (type == Common.MakeKey("vide")) // video trak to get frame rate
            {
                DoVIDE();
            }
            else
            {
                DoMeta();
            }
        }

        private void DoVIDE()
        {
            if (DEBUD) Console.WriteLine("video");
            UInt32 samples = 0;
            UInt32 entries;
            len = mp4.fs.Read(skipb, 0, 4);
            len += mp4.fs.Read(datab, 0, 4);

            UInt32 num = BitConverter.ToUInt32(datab);
            num = Common.ByteSwap32(num);

            if (num <= (size / 8))
            {
                entries = num;

                while (entries > 0)
                {
                    UInt32 samplecount;
                    UInt32 duration;
                    len += mp4.fs.Read(datab, 0, 4);
                    data = BitConverter.ToUInt32(datab);
                    samplecount = Common.ByteSwap32(data);
                    len += mp4.fs.Read(datab, 0, 4);
                    data = BitConverter.ToUInt32(datab);
                    duration = Common.ByteSwap32(data);

                    samples += samplecount;
                    entries--;

                    if (mp4.video_framerate_numerator == 0)
                    {
                        mp4.video_framerate_numerator = mp4.trak_clockdemon;
                        mp4.video_framerate_denominator = duration;
                        mp4.fps = (double)mp4.video_framerate_numerator / (double)mp4.video_framerate_denominator;
                    }

                    if (DEBUD) Console.WriteLine("mp4.trak_clockcount: {0}", mp4.trak_clockcount);
                    if (DEBUD) Console.WriteLine("mp4.trak_clockdemon: {0}", mp4.trak_clockdemon);
                    if (DEBUD) Console.WriteLine("mp4.videolength -> {0}", (double)mp4.trak_clockcount / mp4.trak_clockdemon);
                    if (DEBUD) Console.WriteLine("frames in general: {0}", samples);
                    if (DEBUD) Console.WriteLine("mp4.video_framerate_numerator -> {0}", mp4.video_framerate_numerator);
                    if (DEBUD) Console.WriteLine("mp4.video_framerate_denominator -> {0}", mp4.video_framerate_denominator);
                    if (DEBUD) Console.WriteLine("fps -> {0}", (double)mp4.video_framerate_numerator / (double)mp4.video_framerate_denominator);
                    if (DEBUD) Console.WriteLine("mp4.videolength -> {0}", mp4.videolength);

                }
                mp4.video_frames = samples;
            }

            LongSeek(mp4.fs, size - 8 - (UInt32)len); // skip over stco
        }

        private void DoMeta()
        {
            if (type == traktype) // meta 
            {
                if (DEBUD) Console.WriteLine("meta");
                UInt32 totaldur = 0, samples = 0;
                UInt32 entries = 0;
                len = mp4.fs.Read(skipb, 0, 4);
                len += mp4.fs.Read(datab, 0, 4);
                UInt32 num = BitConverter.ToUInt32(datab);
                num = Common.ByteSwap32(num);

                if (num <= (size / 8))
                {
                    entries = num;

                    mp4.meta_clockdemon = mp4.trak_clockdemon;
                    mp4.meta_clockcount = mp4.trak_clockcount;

                    while (entries > 0)
                    {
                        UInt32 samplecount;
                        UInt32 duration;
                        len += mp4.fs.Read(datab, 0, 4);
                        data = BitConverter.ToUInt32(datab);
                        samplecount = Common.ByteSwap32(data);
                        len += mp4.fs.Read(datab, 0, 4);
                        data = BitConverter.ToUInt32(datab);
                        duration = Common.ByteSwap32(data);

                        samples += samplecount;
                        entries--;

                        totaldur += duration;
                        mp4.metadatalength += (double)((double)samplecount * (double)duration / (double)mp4.meta_clockdemon);
                        if (samplecount > 1 || num == 1)
                            mp4.basemetadataduration = mp4.metadatalength * (double)mp4.meta_clockdemon / (double)samples;

                        if (DEBUD) Console.WriteLine("mp4.meta_clockdemon -> {0}", mp4.meta_clockdemon);
                        if (DEBUD) Console.WriteLine("mp4.meta_clockcount -> {0}", mp4.meta_clockcount);
                        if (DEBUD) Console.WriteLine("mp4.basemetadataduration -> {0}", mp4.basemetadataduration);
                        if (DEBUD) Console.WriteLine("mp4.metadatalength -> {0}", mp4.metadatalength);
                        if (DEBUD) Console.WriteLine("Time is {0}", dateTime.ToString());
                       
                    }
                }

                LongSeek(mp4.fs, size - 8 - (UInt32)len); // skip over stco
            }
            else
                LongSeek(mp4.fs, size - 8);
        }

        #endregion
    }
}
