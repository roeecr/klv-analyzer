﻿using System;
using System.Collections.Generic;
using System.Text;
using Fleck;
using System.Threading;
using System.Net;

namespace KlvLibrary.WebServer
{
    public class KlvWebSocket
    {
        private WebSocketServer server;

        public KlvWebSocket()
        {
            #region old
            //this.connections = new List<Connection>();
            //this.threads = new List<Thread>();
            //this.stopper = false;
            #endregion

            #region WebSocketSharp
            //this.server = new WebSocketServer(IPAddress.Any, 80);
            //this.server.AddWebSocketService<Connection>("/Connection", () => new Connection("maybe connection"));
            //this.server.Start();

            //Console.WriteLine("Web Server is started...");

            //Console.ReadKey(true);
            #endregion

            server = new WebSocketServer("ws://0.0.0.0:80");
            server.Start(socket =>
            {
                Console.WriteLine("Server is waiting");

                socket.OnOpen = () => new Connection(socket);
            });

            Console.ReadKey(true);
        }

        #region old
        //public void Listen()
        //{
        //    while (!this.stopper)
        //    {
        //        Console.WriteLine("Waiting to client...");
        //        TcpClient client = this.server.AcceptTcpClient();
        //        //NetworkStream stream = client.GetStream();


        //        Thread t = new Thread(() => createConnection(client));
        //        t.Start();
        //        this.threads.Add(t);

        //    }
        //}

        //public void createConnection(TcpClient client)
        //{
        //    connections.Add(new Connection(client));
        //}
        #endregion
    }
}
