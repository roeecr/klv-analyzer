﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace KlvLibrary.MpegTsDecoder
{
    public class Utils
    {
        public static bool IsPackedStart(byte[] data)
        {
            return data[0] == 0x47;
        }

        public static UInt32 GetTimestamp(byte[] data, int index)
        {
            //UInt32 timestamp = (UInt32)(data[index] & 0x0E) * 536870912;
            //timestamp += Math.Abs((int)(ReadUInt_BigEndian(data, index + 1, 2) & 0xFFFE) * 16384);
            int timestamp = Math.Abs((int)(data[index] & 0x0E) * 0x20000000);
            timestamp += Math.Abs((int)(ReadUInt_BigEndian(data, index + 1, 2) & 0xFFFE) * 0x4000);
            timestamp += Convert.ToInt32(ReadUInt_BigEndian(data, index + 3, 2) / 2); /*(int)((int)ReadUInt_BigEndian(data, index + 3, 2) / 2);*/
            return Convert.ToUInt32(timestamp);
        }

        public static UInt32 ReadUInt_BigEndian(byte[] data, int index, int len)
        {

            Array.Reverse(data.Skip(index).Take(len).ToArray()) ;
            return BitConverter.ToUInt32(data);
        }

        public static UInt32 ReadUInt32_BigEndian(byte[] data, int index)
        {
            return ReadUInt_BigEndian(data, index, 4);
        }

        public static UInt32 GetOrCreateKey(Dictionary<string, UInt32> dict, string key, UInt32 defaultValue)
        {
            UInt32 value;
            if (dict.ContainsKey(key))
                value = dict[key];

            else
            {
                value = defaultValue;
                dict.Add(key, value);
            }

            return value;
        }
    }
}
