﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using KlvLibrary.Utils;

namespace KlvLibrary.MpegTsDecoder
{
    public class Decode
    {

        public static void DecodeTsFile(string path)
        {
            if (File.Exists(path))
            {
                using (FileStream fs = File.Open(path, FileMode.Open, FileAccess.Read))
                {
                    int counter = 0;
                    int len = 0;
                    byte[] headerb = new byte[4];
                    byte[] adaptionfieldb = new byte[2];
                    PacketHeader packetHeader = new PacketHeader();
                    AdaptionField adaptionField = new AdaptionField();
                    Console.WriteLine("fs length is {0}", fs.Length);

                    do
                    {
                        len = fs.Read(headerb, 0, 4);
                        packetHeader.setHeader(headerb);
                        Console.WriteLine($"-- packet {counter} --");
                        Console.WriteLine(packetHeader.ToString());
                        if (packetHeader.adaptionFieldControl == (byte)HeaderFlags.AdaptionFieldControl.NO_ADAPTION_ONLY_PAYLOAD ||
                            packetHeader.adaptionFieldControl == (byte)HeaderFlags.AdaptionFieldControl.RESERVED_FOR_FUTURE)
                        {
                            Console.WriteLine("No Adaption.");
                        }
                        else
                        {
                            Console.WriteLine("-- Adaption --");
                            len += fs.Read(adaptionfieldb, 0, 2);
                            adaptionField.setHeader(adaptionfieldb);
                            Console.WriteLine(adaptionField.ToString());
                        }

                        #region header
                        //header = BitConverter.ToUInt32(headerb);
                        //header = Common.ByteSwap32(header);

                        //Console.WriteLine($"-- packet {counter} --");
                        ////Console.WriteLine("header -> {0}", );

                        //Console.WriteLine("Sync Byte -> {0:x}", headerb[0]);
                        //Console.WriteLine("Sync Byte -> {0:x}", (header & 0xff000000) >> 24);

                        //Console.WriteLine("Error indicator -> {0:x}", (headerb[1] & 80 ) >> 7);
                        //Console.WriteLine("Error indicator -> {0:x}", (header & 0x800000) >> 23);

                        //Console.WriteLine();

                        //Console.WriteLine("Sync Byte -> {0}", headerb[0]);
                        //Console.WriteLine("Sync Byte -> {0}", headerb[0]);
                        //Console.WriteLine("Sync Byte -> {0}", headerb[0]);
                        //Console.WriteLine("Sync Byte -> {0}", headerb[0]);
                        #endregion

                        counter++;
                        LongSeek(fs, counter);
                        Console.WriteLine();
                     } while (len > 0);
                }
            }

        }
    
        public static void LongSeek(FileStream f, int counter)
        {
            f.Seek(188 * counter, SeekOrigin.Begin);
        }

        //public static void LongSeek(FileStream f, int len)
        //{
        //    f.Seek(188 - len, SeekOrigin.Current);
        //}
    }
}
