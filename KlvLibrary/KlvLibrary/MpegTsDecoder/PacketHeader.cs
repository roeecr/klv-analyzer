﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KlvLibrary.MpegTsDecoder
{
    public class PacketHeader
    {
        public byte syncByte { get; set; }
        public bool transportErrorIndicator { get; set; }
        public bool payloadUnitStartIndicator { get; set; }
        public bool transportPriority { get; set; }
        public UInt16 pid { get; set; }
        public byte trnasportScramblingControl{ get; set; }
        public byte adaptionFieldControl { get; set; }
        public byte continuityCounter{ get; set; }

        public PacketHeader() { }

        public void setHeader(byte[] headerb)
        {
            syncByte = headerb[0];
            transportErrorIndicator = ((headerb[1] & 0x80) >> 7 == 1) ? true : false;
            payloadUnitStartIndicator = ((headerb[1] & 0x40) >> 6 == 1) ? true : false;
            transportPriority = ((headerb[1] & 0x20) >> 5 == 1) ? true : false;
            pid = (UInt16)((headerb[1] & 0x1f) << 8);
            pid = (UInt16)(pid | (UInt16)headerb[2]);
            trnasportScramblingControl = (byte)(headerb[3] & 0xc0 >> 6);
            adaptionFieldControl = (byte)(headerb[3] & 0x30 >> 4);
            continuityCounter = (byte)(headerb[3] & 0xf);
        }

        public override string ToString()
        {
            return String.Format("syncByte -> {0:x}\n" +
                    "transportErrorIndicator -> {1:x}\n" +
                    "PayloadUnitStartIndicator -> {2:x}\n" +
                    "transportPriority -> {3:x}\n" +
                    "pid -> {4:x}\n" +
                    "trnasportScramblingControl -> {5:x}\n" +
                    "adaptionFieldControl -> {6:x}\n" +
                    "continuityCounter -> {7:x}",
                    syncByte, transportErrorIndicator, payloadUnitStartIndicator, transportPriority,
                    pid, trnasportScramblingControl, adaptionFieldControl, continuityCounter);
        }
    }
}
