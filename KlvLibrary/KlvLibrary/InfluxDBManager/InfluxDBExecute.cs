﻿using System;
using System.Threading.Tasks;
using InfluxDB.Client;
using InfluxDB.Client.Writes;
using System.Collections.Generic;
using KlvLibrary.Utils;
using System.Text;

namespace KlvLibrary.InfluxDBManager
{
    public class InfluxDBExecute
    {
        private InfluxDBConnection connection;
        private string measurement;
        private DateTime time;
        private string dateFormat = "yyyy-MM-ddTHH:mm:ss.fffZ";

        public InfluxDBExecute(string device)
        {
            this.connection = InfluxDBConnection.getInstance();
            this.measurement = device;
            this.time = DateTime.UtcNow;
            Console.WriteLine($"InfluxDBExecute created on {this.time.ToString("yyyy-MM-dd HH:mm:ss.fff")}");
        }

        public async void ReadApi()
        {
            Console.WriteLine("*** Query Points ***");

            var query = $"from(bucket: \"{connection.getDatabaseName()}/{connection.getRetentionPolicy()}\") |> range(start: 0)";

            var fluxTables = await connection.queryApi.QueryAsync(query);

            for (int i = 0; i < fluxTables.Count; i++)
            {
                var fluxRecord = fluxTables[i].Records;
                var record = fluxRecord[0];
                string timeRec = record.GetTime().ToString();
                string measurementRec = record.GetMeasurement();
                string fieldRec = record.GetField();
                string valueRec = record.GetValue().ToString();
                string deviceTag = record.GetValueByKey("device").ToString();
                string sensorTag = record.GetValueByKey("sensor").ToString();

                Console.WriteLine($"Time:{timeRec}, Measurement: {measurementRec}. Device -> {deviceTag}, Sensor {sensorTag} -> {valueRec}");
            }

            connection.dispose();
        }

        //public void WriteApi(string sensorName, string value)
        //{
        //    PointData p = PointData.Measurement(this.measurement)
        //        .Tag("sensorName", sensorName)
        //        .Field("value", value)
        //        .Timestamp(DateTime.UtcNow, InfluxDB.Client.Api.Domain.WritePrecision.Ns);
        //        //.Timestamp(DateTime.UtcNow.AddMilliseconds(DateTime.UtcNow.Millisecond), InfluxDB.Client.Api.Domain.WritePrecision.Ns);

        //    this.connection.writeApi.WritePoint(p);

        //}

        public void WriteApi(string sensorTag, string value, string sensorName, double offset)
        {
            PointData p = PointData.Measurement(this.measurement)
                .Tag("sensorKey", sensorTag)
                .Field("value", value)
                .Field("offset", offset)
                .Field("sensorName", sensorName)
                .Timestamp(DateTime.UtcNow, InfluxDB.Client.Api.Domain.WritePrecision.Ns);
            //.Timestamp(DateTime.UtcNow.AddMilliseconds(DateTime.UtcNow.Millisecond), InfluxDB.Client.Api.Domain.WritePrecision.Ns);

            this.connection.writeApi.WritePoint(p);

        }

        public void WriteKlvObject(KlvObj klv)
        {
            for (int i = 0; i < klv.datas.Count; i++)
            {
                SensorData sensor = klv.datas[i];
                double timeOffset = klv.timeOffset; 
                WriteApi(sensor.key, sensor.data, sensor.name, timeOffset);
            }
        }

        public async Task<DateTime> GetAnnotationByOffset(double offset)
        {
            string start = this.time.ToString(dateFormat);

            var query = $"from(bucket: \"{connection.getDatabaseName()}/{connection.getRetentionPolicy()}\")\n"
                + $" |> range(start: {start}, stop: now())"
                + $" |> filter(fn: (r) => (r[\"_measurement\"] == \"{this.measurement}\" and r[\"_field\"] == \"offset\" and r[\"_value\"] <= {offset}))"
                + $" |> sort(columns:[\"_value\"], desc: true)"
                + $" |> limit(n: 1)";


            var fluxTables = await connection.queryApi.QueryAsync(query);
            DateTime maxTime = this.time;

            for (int i = 0; i < fluxTables.Count; i++)
            {
                try
                {
                    var records = fluxTables[i].Records;

                    var record = records[0];

                    DateTime timeRec = ((DateTime)record.GetTimeInDateTime());

                    if (timeRec > maxTime)
                        maxTime = timeRec;
                    
                }
                catch
                {
                    Console.WriteLine($"Error in record {0}, requested offset: {offset}");
                }

            }

            return maxTime;
        }

        public async Task<List<Annotation>> ReadAllAnotations(DateTime anotationTime)
        {
            string start = this.time.AddMilliseconds(-1).ToString(dateFormat);
            string stop = anotationTime.AddMilliseconds(1).ToString(dateFormat);


            var query = $"from(bucket: \"{connection.getDatabaseName()}/{connection.getRetentionPolicy()}\")\n"
                + $" |> range(start: {start}, stop: {stop})"
                + $" |> filter(fn: (r) => (r[\"_measurement\"] == \"{this.measurement}\" and r[\"_field\"] == \"value\"))"
                + $" |> sort(columns:[\"_time\"], desc: true)"
                + $" |> limit(n: 1)";

            var fluxTables = await connection.queryApi.QueryAsync(query);
            //Dictionary<string, string> telemetry = new Dictionary<string, string>();
            List<Annotation> telemetry = new List<Annotation>();

            for (int i = 0; i < fluxTables.Count; i++)
            {
                try
                {
                    var records = fluxTables[i].Records;

                    var record = records[0];
                    string sensorValue = record.GetValue().ToString();
                    string sensorKey = record.GetValueByKey("sensorKey").ToString();

                    telemetry.Add(new Annotation(sensorKey, sensorValue));
                }
                catch
                {
                    Console.WriteLine($"Error in record {0}, requested time: {anotationTime}");
                }
            }

            return telemetry;

        }

    }
}
