﻿//using System;
//using System.IO;
//using System.Text;
//using KlvLibrary.Utils;

//namespace KlvLibrary
//{
//    public class OpenFileWork
//    {
//        public static bool DEBUD = false;

//        private DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, 0);

//        public OpenFileWork()
//        {

//        }

//        public Mp4Object openMP4(string path, UInt32 traktype, UInt32 traksubtype)
//        {
//            #region binary reader
//            //BinaryReader reader = new BinaryReader(fs);


//            //int len = 0;
//            //UInt32 size = reader.ReadUInt32(); len += 4;
//            //UInt32 tag = reader.ReadUInt32(); len += 4;


//            //if (len == 8 && tag != Utils.Utils.MakeKey("ftyp"))
//            //{
//            //    Console.WriteLine("first header should be ftyp but wasn't");
//            //}
//            //else
//            //{
//            //    size = Utils.Utils.ByteSwap32(size);
//            //    Console.WriteLine("ftype byte -> {0:x}", Utils.Utils.MakeKey("ftyp"));
//            //    Console.WriteLine("answer is {0:x}, {1:x}", size, tag);
//            //    Console.WriteLine("fs position is: {0}", fs.Position);
//            //}
//            #endregion

//            if (File.Exists(path))
//            {
//                Mp4Object mp4 = new Mp4Object(path);

//                int len = 0;
//                byte[] sizeb = new byte[4];
//                byte[] tagb = new byte[4];
//                byte[] skipb = new byte[4];
//                byte[] datab = new byte[4];
//                byte[] subtypeb = new byte[4];
//                UInt32 type = 0;

//                //Console.WriteLine("fs length is {0}", mp4.fs.Length);

//                do
//                {
//                    len = mp4.fs.Read(sizeb, 0, 4);
//                    len += mp4.fs.Read(tagb, 0, 4);

//                    UInt32 size = BitConverter.ToUInt32(sizeb);
//                    UInt32 tag = BitConverter.ToUInt32(tagb);



//                    UInt32 data;

//                    //Console.WriteLine("value of 'size' byte array is {0:x}", size);
//                    //Console.WriteLine("value of 'tag' byte array is {0:x}", tag);
//                    //Console.WriteLine("ftype -> {0:x}", Utils.Utils.MakeKey("ftyp"));
//                    if (len == 8 && mp4.fs.Position < mp4.fs.Length)
//                    {
//                        //Console.WriteLine(Common.Key2FourCC(tag));
//                        if (mp4.fs.Position == 8 && tag != Utils.Utils.MakeKey("ftyp"))
//                        {
//                            //Console.WriteLine("first header should be ftyp but wasn't");
//                            break;
//                        }

//                        size = Utils.Utils.ByteSwap32(size);
//                        //Console.WriteLine("ftype byte -> {0:x}", Utils.Utils.MakeKey("ftyp"));
//                        //Console.WriteLine("answer is {0:x} -> {1}, {2:x}", size, size, tag);
//                        //Console.WriteLine("fs position is: {0}", fs.Position);

//                        //Console.WriteLine("tag -> {0}, size {1}", Utils.Common.Key2FourCC(tag), size);

//                        if (this.IsUnnecessaryTag(tag))
//                        {
//                            LongSeek(mp4.fs, size - 8);

//                            //Console.WriteLine("fs position {0}", fs.Position);
//                        }
//                        else
//                        {
//                            if (tag == Utils.Utils.MakeKey("mvhd"))
//                            {

//                                if (DEBUD) Console.WriteLine("mvhd");
//                                len = mp4.fs.Read(skipb, 0, 4);
//                                len += mp4.fs.Read(skipb, 0, 4);
//                                len += mp4.fs.Read(skipb, 0, 4);

//                                len += mp4.fs.Read(datab, 0, 4);
//                                data = BitConverter.ToUInt32(datab);
//                                mp4.clockdemon = Utils.Utils.ByteSwap32(data);
//                                if (DEBUD) Console.WriteLine("clockdemon is : {0}", mp4.clockdemon);

//                                len += mp4.fs.Read(datab, 0, 4);
//                                data = BitConverter.ToUInt32(datab);
//                                mp4.clockcount = Utils.Utils.ByteSwap32(data);

//                                if (DEBUD) Console.WriteLine("clockcount is : {0}", mp4.clockcount);

//                                LongSeek(mp4.fs, size - 8 - (UInt32)len); // skip over mvhd
//                            }
//                            else if (tag == Utils.Utils.MakeKey("trak"))
//                            {
//                                mp4.trak_num++;
//                                if (DEBUD) Console.WriteLine("trak {0}", mp4.trak_num);
//                            }
//                            else if (tag == Utils.Utils.MakeKey("mdhd"))
//                            {
//                                if (DEBUD) Console.WriteLine("mdhd");
//                                len = mp4.fs.Read(skipb, 0, 4);
//                                len += mp4.fs.Read(datab, 0, 4);
//                                data = BitConverter.ToUInt32(datab);
//                                data = Utils.Utils.ByteSwap32(data);
//                                if (DEBUD) Console.WriteLine("creation time is : {0}", data);

//                                if (DEBUD) Console.WriteLine("seconds {0}", data);
//                                dateTime = dateTime.AddSeconds(data / 10);

//                                len += mp4.fs.Read(datab, 0, 4);
//                                data = BitConverter.ToUInt32(datab);
//                                data = Utils.Utils.ByteSwap32(data);
//                                if (DEBUD) Console.WriteLine("modification time is : {0}", data);

//                                len += mp4.fs.Read(datab, 0, 4);
//                                data = BitConverter.ToUInt32(datab);
//                                mp4.trak_clockdemon = Utils.Utils.ByteSwap32(data);
//                                if (DEBUD) Console.WriteLine("time scale is : {0:x}", mp4.trak_clockdemon);

//                                len += mp4.fs.Read(datab, 0, 4);
//                                data = BitConverter.ToUInt32(datab);
//                                mp4.trak_clockcount = Utils.Utils.ByteSwap32(data);
//                                if (DEBUD) Console.WriteLine("duration is : {0}", mp4.trak_clockcount);

//                                if (mp4.videolength == 0.0)
//                                {

//                                    mp4.videolength = (double)mp4.trak_clockcount / mp4.trak_clockdemon;
//                                    if (DEBUD) Console.WriteLine("video length {0}", mp4.videolength);
//                                }


//                                LongSeek(mp4.fs, size - 8 - (UInt32)len);
//                            }
//                            else if (tag == Utils.Utils.MakeKey("hdlr"))
//                            {
//                                // Console.WriteLine("hdlr");
//                                len = mp4.fs.Read(skipb, 0, 4);
//                                len += mp4.fs.Read(skipb, 0, 4);
//                                len += mp4.fs.Read(datab, 0, 4);  // type will be 'meta' for the correct trak.

//                                UInt32 temp = BitConverter.ToUInt32(datab);
//                                if (DEBUD) Console.WriteLine("temp is {0}", temp);
//                                //Console.WriteLine("temp is {0}", Utils.Common.Key2FourCC(temp));

//                                if (temp != Utils.Utils.MakeKey("alis") && temp != Utils.Utils.MakeKey("url "))
//                                    type = temp;


//                                LongSeek(mp4.fs, size - 8 - (UInt32)len); // skip over hldr
//                            }
//                            else if (tag == Utils.Utils.MakeKey("edts"))
//                            {
//                                if (DEBUD) Console.WriteLine("edts");
//                                UInt32 elst, temp, readnum;

//                                len = mp4.fs.Read(skipb, 0, 4);
//                                len += mp4.fs.Read(datab, 0, 4);
//                                elst = BitConverter.ToUInt32(datab);

//                                if (elst == Utils.Utils.MakeKey("elst"))
//                                {
//                                    if (DEBUD) Console.WriteLine("elst");
//                                    len += mp4.fs.Read(datab, 0, 4);
//                                    temp = BitConverter.ToUInt32(datab);

//                                    if (temp == 0)
//                                    {
//                                        len += mp4.fs.Read(datab, 0, 4);
//                                        readnum = BitConverter.ToUInt32(datab);
//                                        readnum = Utils.Utils.ByteSwap32(readnum);

//                                        if (DEBUD) Console.WriteLine("readnum -> {0}", readnum);

//                                        if (readnum >= (size / 12) && mp4.trak_clockdemon > 0)
//                                        {
//                                            UInt32 segment_duration, segment_mediaTime, segment_mediaRate;
//                                            for (int i = 0; i < readnum; i++)
//                                            {
//                                                len += mp4.fs.Read(datab, 0, 4);
//                                                segment_duration = BitConverter.ToUInt32(datab);
//                                                segment_duration = Utils.Utils.ByteSwap32(segment_duration);
//                                                if (DEBUD) Console.WriteLine("segment duration of {0} is {1}", i, segment_duration);

//                                                len += mp4.fs.Read(datab, 0, 4);
//                                                segment_mediaTime = BitConverter.ToUInt32(datab);
//                                                segment_mediaTime = Utils.Utils.ByteSwap32(segment_mediaTime);
//                                                if (DEBUD) Console.WriteLine("segment mediaTime of {0} is {1}", i, segment_mediaTime);

//                                                len += mp4.fs.Read(datab, 0, 4);
//                                                segment_mediaRate = BitConverter.ToUInt32(datab);
//                                                segment_mediaRate = Utils.Utils.ByteSwap32(segment_mediaRate);
//                                                if (DEBUD) Console.WriteLine("segment mediaRate of {0} is {1}", i, segment_mediaRate);

//                                                if (segment_mediaTime == 0xffffffff) // the segment_duration for blanked time
//                                                    mp4.trak_edit_list_offsets[mp4.trak_num] += (UInt32)segment_duration;  //samples are delay, data starts after presentation time zero.
//                                                else if (i == 0) // If the first editlst starts after zero, the track is offset by this time (time before presentation time zero.)
//                                                    mp4.trak_edit_list_offsets[mp4.trak_num] -= (uint)((double)segment_mediaTime / (double)mp4.trak_clockdemon * (double)mp4.clockdemon); //convert to M

//                                                if (DEBUD) Console.WriteLine("trak edit list offsets {0}", mp4.trak_edit_list_offsets[mp4.trak_num]);
//                                            }
//                                            if (type == traktype) // GPMF metadata
//                                            {
//                                                mp4.metadataoffset_clockcount = mp4.trak_edit_list_offsets[mp4.trak_num]; //leave in MP4 clock base
//                                                if (DEBUD) Console.WriteLine("metadataoffset_clockcount {0}", mp4.metadataoffset_clockcount);
//                                            }
//                                        }
//                                    }
//                                }

//                                LongSeek(mp4.fs, size - 8 - (UInt32)len);
//                            }
//                            else if (tag == Utils.Utils.MakeKey("stsd"))
//                            {
//                                if (DEBUD) Console.WriteLine("stsd");
//                                if (type == traktype) //like meta
//                                {
//                                    len = mp4.fs.Read(skipb, 0, 4);
//                                    len += mp4.fs.Read(skipb, 0, 4);
//                                    len += mp4.fs.Read(skipb, 0, 4);
//                                    len += mp4.fs.Read(subtypeb, 0, 4);  // type will be 'meta' for the correct trak.

//                                    UInt32 subtype = BitConverter.ToUInt32(subtypeb);

//                                    if (DEBUD) Console.WriteLine("subtype -> {0}", subtype);

//                                    if (len == 16)
//                                    {
//                                        if (subtype != traksubtype) // not MP4 metadata 
//                                        {
//                                            type = 0; // MP4
//                                        }
//                                    }

//                                    LongSeek(mp4.fs, size - 8 - (UInt32)len); // skip over stsd
//                                }
//                                else
//                                    LongSeek(mp4.fs, size - 8);
//                            }
//                            else if (tag == Utils.Utils.MakeKey("stsc"))
//                            {
//                                if (DEBUD) Console.WriteLine("stsc");
//                                //if (type == traktype)
//                                //{
//                                //    len = mp4.fs.Read(skipb, 0, 4);
//                                //    len += mp4.fs.Read(datab, 0, 4);
//                                //    UInt32 num = BitConverter.ToUInt32(datab);
//                                //    num = Utils.Utils.ByteSwap32(num);

//                                //    if (num )
//                                //}
//                                LongSeek(mp4.fs, size - 8); // skip over stsx
//                            }
//                            else if (tag == Utils.Utils.MakeKey("stsz"))
//                            {
//                                if (DEBUD) Console.WriteLine("stsz");

//                                if (type == traktype) // meta
//                                {
//                                    UInt32 equalsamplesize, num;

//                                    len = mp4.fs.Read(skipb, 0, 4);
//                                    len += mp4.fs.Read(datab, 0, 4);
//                                    equalsamplesize = BitConverter.ToUInt32(datab);

//                                    len += mp4.fs.Read(datab, 0, 4);
//                                    num = BitConverter.ToUInt32(datab);
//                                    num = Utils.Utils.ByteSwap32(num);

//                                    if (DEBUD) Console.WriteLine("num is {0}\nequalsample is {1}", num, equalsamplesize);

//                                    // if equalsamplesize != 0, it is the size of all the samples and the length should be 20 (size,fourcc,flags,samplesize,samplecount)
//                                    if ((num <= (size / sizeof(UInt32))) || (equalsamplesize != 0 && size == 20))
//                                    {
//                                        //either the samples are different sizes or they are all the same size
//                                        if (num > 0 && ((size > (num * 4)) || (equalsamplesize != 0 && size == 20)))
//                                        {
//                                            mp4.metasize_count = num;
//                                            mp4.metasizes = new UInt32[num * 4];

//                                            if (equalsamplesize == 0)
//                                            {
//                                                for (int i = 0; i < num; i++)
//                                                {
//                                                    len += mp4.fs.Read(datab, 0, 4);
//                                                    mp4.metasizes[i] = BitConverter.ToUInt32(datab);
//                                                    mp4.metasizes[i] = Utils.Utils.ByteSwap32(mp4.metasizes[i]);
//                                                    //if (DEBUD)
//                                                    if (DEBUD) Console.WriteLine("meta size of {0} is {1}", i, mp4.metasizes[i]);
//                                                }
//                                            }
//                                            else
//                                            {
//                                                equalsamplesize = Utils.Utils.ByteSwap32(equalsamplesize);
//                                                do
//                                                {
//                                                    num--;
//                                                    mp4.metasizes[num] = equalsamplesize;
//                                                    //if (DEBUD) 
//                                                    if (DEBUD) Console.WriteLine("meta size of {0} is {1}", num, mp4.metasizes[num]);
//                                                } while (num > 0);
//                                            }
//                                            if (DEBUD) Console.WriteLine("metasize_count: {0}", mp4.metasize_count);

//                                        }
//                                        else
//                                        {
//                                            //size of null
//                                            if (DEBUD) Console.WriteLine("break out - size of null");
//                                            break;
//                                        }
//                                    }
//                                    LongSeek(mp4.fs, size - 8 - (UInt32)len); // skip over stsz
//                                }
//                                else
//                                    LongSeek(mp4.fs, size - 8);
//                            }
//                            else if (tag == Utils.Utils.MakeKey("stco"))
//                            {
//                                if (DEBUD) Console.WriteLine("stco");
//                                if (type == traktype) // meta
//                                {
//                                    len = mp4.fs.Read(skipb, 0, 4);
//                                    len += mp4.fs.Read(datab, 0, 4);
//                                    UInt32 num = BitConverter.ToUInt32(datab);
//                                    num = Utils.Utils.ByteSwap32(num);

//                                    if (DEBUD) Console.WriteLine("num -> {0}", num);

//                                    if (num <= ((size - 8 - len) / sizeof(UInt32)))
//                                    {
//                                        mp4.metastco_count = num;
//                                        if (DEBUD) Console.WriteLine("mp4.metastco_count -> {0}", mp4.metastco_count);
//                                    }

//                                    mp4.metaoffsets = new UInt32[num];
//                                    if (mp4.metaoffsets != null)
//                                    {
//                                        for (int i = 0; i < num; i++)
//                                        {
//                                            int readlen = mp4.fs.Read(datab, 0, 4);
//                                            data = BitConverter.ToUInt32(datab);
//                                            mp4.metaoffsets[i] = Utils.Utils.ByteSwap32(data);
//                                            if (DEBUD) Console.WriteLine("meta offsets of {0} is {1}", i, mp4.metaoffsets[i]);
//                                            len += readlen;
//                                        }

//                                    }

//                                    LongSeek(mp4.fs, size - 8 - (UInt32)len); // skip over stco
//                                }
//                                else
//                                    LongSeek(mp4.fs, size - 8); // skip over stco

//                            }


//                            else if (tag == Utils.Utils.MakeKey("stts"))
//                            {
//                                if (DEBUD) Console.WriteLine("stts");

//                                if (type == Utils.Common.MakeKey("vide")) // video trak to get frame rate
//                                {
//                                    if (DEBUD) Console.WriteLine("video");
//                                    UInt32 samples = 0;
//                                    UInt32 entries;
//                                    len = mp4.fs.Read(skipb, 0, 4);
//                                    len += mp4.fs.Read(datab, 0, 4);

//                                    UInt32 num = BitConverter.ToUInt32(datab);
//                                    num = Utils.Utils.ByteSwap32(num);

//                                    if (num <= (size / 8))
//                                    {
//                                        entries = num;

//                                        while (entries > 0)
//                                        {
//                                            UInt32 samplecount;
//                                            UInt32 duration;
//                                            len += mp4.fs.Read(datab, 0, 4);
//                                            data = BitConverter.ToUInt32(datab);
//                                            samplecount = Utils.Utils.ByteSwap32(data);
//                                            len += mp4.fs.Read(datab, 0, 4);
//                                            data = BitConverter.ToUInt32(datab);
//                                            duration = Utils.Utils.ByteSwap32(data);

//                                            samples += samplecount;
//                                            entries--;

//                                            if (mp4.video_framerate_numerator == 0)
//                                            {
//                                                mp4.video_framerate_numerator = mp4.trak_clockdemon;
//                                                mp4.video_framerate_denominator = duration;
//                                            }

//                                            if (DEBUD) Console.WriteLine("mp4.trak_clockcount: {0}", mp4.trak_clockcount);
//                                            if (DEBUD) Console.WriteLine("mp4.trak_clockdemon: {0}", mp4.trak_clockdemon);
//                                            if (DEBUD) Console.WriteLine("mp4.videolength -> {0}", (double)mp4.trak_clockcount / mp4.trak_clockdemon);
//                                            if (DEBUD) Console.WriteLine("frames in general: {0}", samples);
//                                            if (DEBUD) Console.WriteLine("mp4.video_framerate_numerator -> {0}", mp4.video_framerate_numerator);
//                                            if (DEBUD) Console.WriteLine("mp4.video_framerate_denominator -> {0}", mp4.video_framerate_denominator);
//                                            if (DEBUD) Console.WriteLine("fps -> {0}", (double)mp4.video_framerate_numerator / (double)mp4.video_framerate_denominator);
//                                            if (DEBUD) Console.WriteLine("mp4.videolength -> {0}", mp4.videolength);
//                                        }
//                                        mp4.video_frames = samples;
//                                    }

//                                    LongSeek(mp4.fs, size - 8 - (UInt32)len); // skip over stco
//                                }
//                                else
//                                {
//                                    if (type == traktype) // meta 
//                                    {
//                                        if (DEBUD) Console.WriteLine("meta");
//                                        UInt32 totaldur = 0, samples = 0;
//                                        UInt32 entries = 0;
//                                        len = mp4.fs.Read(skipb, 0, 4);
//                                        len += mp4.fs.Read(datab, 0, 4);
//                                        UInt32 num = BitConverter.ToUInt32(datab);
//                                        num = Utils.Utils.ByteSwap32(num);

//                                        if (num <= (size / 8))
//                                        {
//                                            entries = num;

//                                            mp4.meta_clockdemon = mp4.trak_clockdemon;
//                                            mp4.meta_clockcount = mp4.trak_clockcount;

//                                            while (entries > 0)
//                                            {
//                                                UInt32 samplecount;
//                                                UInt32 duration;
//                                                len += mp4.fs.Read(datab, 0, 4);
//                                                data = BitConverter.ToUInt32(datab);
//                                                samplecount = Utils.Utils.ByteSwap32(data);
//                                                len += mp4.fs.Read(datab, 0, 4);
//                                                data = BitConverter.ToUInt32(datab);
//                                                duration = Utils.Utils.ByteSwap32(data);

//                                                samples += samplecount;
//                                                entries--;

//                                                totaldur += duration;
//                                                mp4.metadatalength += (double)((double)samplecount * (double)duration / (double)mp4.meta_clockdemon);
//                                                if (samplecount > 1 || num == 1)
//                                                    mp4.basemetadataduration = mp4.metadatalength * (double)mp4.meta_clockdemon / (double)samples;

//                                                if (DEBUD) Console.WriteLine("mp4.meta_clockdemon -> {0}", mp4.meta_clockdemon);
//                                                if (DEBUD) Console.WriteLine("mp4.meta_clockcount -> {0}", mp4.meta_clockcount);
//                                                if (DEBUD) Console.WriteLine("mp4.basemetadataduration -> {0}", mp4.basemetadataduration);
//                                                if (DEBUD) Console.WriteLine("mp4.metadatalength -> {0}", mp4.metadatalength);
//                                                if (DEBUD) Console.WriteLine("Time is {0}", dateTime.ToString());

//                                            }
//                                        }

//                                        LongSeek(mp4.fs, size - 8 - (UInt32)len); // skip over stco
//                                    }
//                                    else
//                                        LongSeek(mp4.fs, size - 8);
//                                }
//                            }
//                            else
//                            {
//                                //Console.WriteLine("else");
//                                // NESTSIZE(8);
//                            }
//                        }
//                    }
//                    else
//                    {
//                        break;
//                    }
//                } while (len > 0);

//                mp4.indexcount = mp4.metasize_count;
//                if (DEBUD) Console.WriteLine("end of file");
//                return mp4;
//            }
//            else
//            {
//                if (DEBUD) Console.WriteLine("not found");
//            }
//            return null;
//        }

//        #region UDTA

//        public Mp4Object openUDTA(string path)
//        {
//            if (File.Exists(path))
//            {
//                Mp4Object mp4 = new Mp4Object(path);

//                UInt64 lastsize = 0;
//                int len = 0;
//                byte[] sizeb = new byte[4];
//                byte[] tagb = new byte[4];
//                byte[] skipb = new byte[4];
//                byte[] datab = new byte[4];
//                byte[] subtypeb = new byte[4];
//                UInt32 type = 0;

//                if (DEBUD) Console.WriteLine("fs length is {0}", mp4.fs.Length);

//                do
//                {
//                    len = mp4.fs.Read(sizeb, 0, 4);
//                    len += mp4.fs.Read(tagb, 0, 4);

//                    if (len == 8)
//                    {
//                        //UInt32 size = BitConverter.ToUInt32(sizeb);
//                        UInt32 tag = BitConverter.ToUInt32(tagb);
//                        //size = Common.ByteSwap32(size);
//                        UInt32 data;
//                        string key = Common.Key2FourCC(tag);



//                        //Console.WriteLine("size -> {0}", size);
//                        //Console.WriteLine("key -> {0}", key);

//                        if (key == "GPMF" || key == "mdat" || key == "ftyp" || key == "moov" || key == "udta")
//                        {
//                            if (DEBUD) Console.WriteLine("here");
//                        }

//                        //if (!Common.IsValidFourCC(key) && tag != 0x7a7978a9)
//                        //{
//                        //    Console.WriteLine("Invalid fourcc");
//                        //    LongSeekUDTA(mp4.fs, (Int64)lastsize - 8 - (Int64)len);
//                        //}
//                    }
//                    //Console.WriteLine("file location {0}", mp4.fs.Position);
//                } while (len > 0);
//            }
//            else
//            {
//                Console.WriteLine("not found");
//            }
//            return null;
//        }


//        public void LongSeekUDTA(FileStream fs, Int64 offest)
//        {
//            if (fs.Position + (UInt32)offest < fs.Length)
//                fs.Seek(offest, SeekOrigin.Current);
//            else
//            {
//                //fs.Seek(fs.Length, SeekOrigin.Begin);
//            }

//        }

//        #endregion

//        public bool IsUnnecessaryTag(UInt32 tag)
//        {
//            return tag != Utils.Utils.MakeKey("moov") && //skip over all but these atoms
//                    tag != Utils.Utils.MakeKey("mvhd") &&
//                    tag != Utils.Utils.MakeKey("trak") &&
//                    tag != Utils.Utils.MakeKey("mdia") &&
//                    tag != Utils.Utils.MakeKey("mdhd") &&
//                    tag != Utils.Utils.MakeKey("minf") &&
//                    tag != Utils.Utils.MakeKey("gmin") &&
//                    tag != Utils.Utils.MakeKey("dinf") &&
//                    tag != Utils.Utils.MakeKey("alis") &&
//                    tag != Utils.Utils.MakeKey("stsd") &&
//                    tag != Utils.Utils.MakeKey("stbl") &&
//                    tag != Utils.Utils.MakeKey("stts") &&
//                    tag != Utils.Utils.MakeKey("stsc") &&
//                    tag != Utils.Utils.MakeKey("stsz") &&
//                    tag != Utils.Utils.MakeKey("stco") &&
//                    tag != Utils.Utils.MakeKey("hdlr") &&
//                    tag != Utils.Utils.MakeKey("edts");
//        }

//        public void LongSeek(FileStream fs, UInt32 offest)
//        {
//            fs.Seek(offest, SeekOrigin.Current);
//        }

//        #region Mp4Object Access Functions

//        public UInt32 GetNumberPayloads(Mp4Object mp4)
//        {
//            return mp4.indexcount;
//        }

//        public byte[] GetPayload(Mp4Object mp4, int index)
//        {
//            if (index < mp4.indexcount && mp4.fs != null)
//            {
//                if ((UInt64)mp4.fs.Length >= mp4.metaoffsets[index] + mp4.metasizes[index] && mp4.metasizes[index] > 0)
//                {
//                    if (DEBUD) Console.WriteLine("meta offset -> {0}\nmeta size -> {1}", mp4.metaoffsets[index], mp4.metasizes[index]);
//                    mp4.fs.Seek((UInt32)mp4.metaoffsets[index], SeekOrigin.Begin);
//                    byte[] buffer = new byte[mp4.metasizes[index]];
//                    mp4.fs.Read(buffer, 0, (int)mp4.metasizes[index]);

//                    return buffer;
//                }

//                else
//                {
//                    Console.WriteLine("here1");
//                }
//            }

//            Console.WriteLine("here2");
//            return null;
//        }

//        public UInt32 GetPayloadSize(Mp4Object mp4, int index)
//        {
//            if (mp4.metasizes != null && mp4.metasize_count > index)
//                return mp4.metasizes[index];

//            return 0;
//        }

//        public float GetDuration(Mp4Object mp4)
//        {
//            if (mp4 != null)
//                return (float)mp4.metadatalength;

//            return 0;
//        }

//        public UInt32 GetFramesNumber(Mp4Object mp4)
//        {
//            return mp4.video_frames;
//        }

//        public UInt32 GetFps(Mp4Object mp4)
//        {
//            return mp4.video_framerate_numerator / mp4.video_framerate_denominator;
//        }

//        #endregion
//    }
//}
