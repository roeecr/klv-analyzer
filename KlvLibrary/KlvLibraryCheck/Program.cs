﻿using KlvLibrary;
using KlvLibrary.Utils;
//using KlvLibrary.Server;
using KlvLibrary.WebServer;
using System.IO;
using System;
using KlvLibrary.MpegTsDecoder;
using System.Linq;

namespace KlvLibraryCheck
{
    class Program
    {
        static void Main(string[] args)
        {

            #region byteswap
            //Console.WriteLine("Hello World!");

            //UInt16 word = 0x10cf;

            //Console.WriteLine("bytes swap of {0:x} -> {1:x}", word, Utils.ByteSwap16(word));

            //UInt32 dbword = 0x11121314;

            //Console.WriteLine("bytes swap of {0:x} -> {1:x}", dbword, Utils.ByteSwap32(dbword));

            //string key = "DEMO";

            //Console.WriteLine("key of {0} -> {1:x}", key, Utils.MakeKey(key));

            //UInt32 word = 0x6201000f;
            //word = Common.ByteSwap32(word);
            //Console.WriteLine("type -> {0}, int -> {1}", Common.SampleType(word), (int)Common.GPMF_SampleType.GPMF_TYPE_SIGNED_BYTE);
            //Console.WriteLine("sample size -> {0}", Common.SampleSize(word));
            //Console.WriteLine("sample repeat -> {0}", Common.SampleRepeat(word));
            #endregion

            #region keys
            //Console.WriteLine("ftype -> {0:x}", Utils.MakeKey("ftyp"));
            //Console.WriteLine("mp41 -> {0:x}", Utils.MakeKey("mp41"));
            //Console.WriteLine("mdat -> {0:x}", Utils.MakeKey("mdat"));
            //Console.WriteLine("moov -> {0:x}", Utils.MakeKey("moov"));
            //Console.WriteLine("trak -> {0:x}", Utils.MakeKey("trak"));
            //Console.WriteLine("vide -> {0:x}", Utils.MakeKey("vide"));
            //Console.WriteLine("soun -> {0:x}", Utils.MakeKey("soun"));
            //Console.WriteLine("tmcd -> {0:x}", Utils.MakeKey("tmcd"));
            //Console.WriteLine("meta -> {0:x}", Utils.MakeKey("meta"));
            #endregion

            #region time
            /*System.DateTime d = new System.DateTime(1970, 1, 1, 0, 0, 0, 0, 0);

            Console.WriteLine("time is {0}", d.ToString());
            d = d.AddSeconds(1113211532);
            Console.WriteLine("time is {0}", d.ToString());*/
            #endregion

            #region openFile

            //string path = "C:/Users/p7586381/Desktop/roee/.net/C#/KlvLibrary/KlvLibraryCheck/samples/hero7.mp4";
            //string path = "D:/Roee/ArmyProject/Roee/C#/18.11.2020/C#/KlvLibrary/KlvLibraryCheck/samples/hero7.mp4";

            //KlvLibrary.OpenFile.openMP4(path, Utils.MakeKey("meta"), Utils.MakeKey("gpmd"));

            #endregion

            #region klv consumer

            //string path = "C:/Users/p7586381/Desktop/roee/.net/30.11.2020/C#/KlvLibrary/KlvLibraryCheck/samples/hero7.mp4";
            ////string path = "C:/Users/p7586381/Desktop/camera-live/out0.mp4";
            ////string path = "D:/Roee/ArmyProject/Roee/C#/25.11.2020/C#/KlvLibrary/KlvLibraryCheck/samples/hero7.mp4";

            //KlvConsumer consumer = new KlvConsumer(path);
            //consumer.CreateKLVObjectsFromFile();

            #endregion

            #region server
            KlvWebSocket webSocket = new KlvWebSocket();
            #endregion

            #region Point Format
            //double x1 = 1.55920384092834;
            //double x2 = 1.52;
            ////Console.WriteLine(((x1 % 1) * 100000) % 10);

            //if (IsLongNumber(x1))
            //    Console.WriteLine(x1.ToString("F4"));
            //else
            //    Console.WriteLine(x1);

            //if (IsLongNumber(x2))
            //    Console.WriteLine(x2.ToString("F4"));
            //else
            //    Console.WriteLine(x2);

            ////Console.WriteLine(Convert.ToSingle(x).ToString("F4"));

            //double d1 = 1.099999998000000001;
            //IsLongNumber(d1);
            //double d2 = 0.289999901;
            //IsLongNumber(d2);
            #endregion

            #region Transport Stream

            //string path = "C:/Users/p7586381/Desktop/roee/.net/7.12.2020/C#/KlvLibrary/KlvLibraryCheck/samples/hero7.ts";
            //////string path = "C:/Users/p7586381/Desktop/camera-live/out0.mp4";
            //////string path = "D:/Roee/ArmyProject/Roee/C#/25.11.2020/C#/KlvLibrary/KlvLibraryCheck/samples/hero7.mp4";


            //Decode.DecodeTsFile(path);

            #endregion

            #region types
            //Object oString = getString(), oInt = getInt(), oFloat = getfloat();
            //Console.WriteLine(oString.ToString());
            //Console.WriteLine(oInt.ToString());
            //Console.WriteLine(oFloat.ToString());

            //Type o = getInfo();
            //if (o == typeof(object))
            //    Console.WriteLine("object");
            //else
            //{
            //    Console.WriteLine("not object");
            //}
            #endregion
        }

        #region Point Format
        //public static bool IsLongNumber(double x)
        //{
        //    double point = (x % 1) * 100000;
        //    Console.WriteLine(point);
        //    Console.WriteLine(point % 10);
        //    return point % 10 > 0;
        //}

        //public static double UptoTwoDecimalPoints(double num)
        //{
        //    var totalCost = Convert.ToSingle(String.Format("{0:0.00}", num));
        //    return totalCost;
        //}

        //public static double IsLongNumber(double x)
        //{
        //    double point = Math.Floor(10000 * x) / 10000;
        //    string s = point.ToString("N4");
        //    double number = Convert.ToDouble(s);
        //    Console.WriteLine("{0} {1}", s, number);
        //    return number;
        //}
        #endregion

        #region type
        //public static object getString()
        //{
        //    return "hey";
        //}

        //public static object getInt()
        //{
        //    return 2;
        //}

        //public static object getfloat()
        //{
        //    return 5.5;
        //}

        //public static Type getInfo()
        //{
        //    return typeof(object);

        //}
        #endregion

        #region switch type of
        /*
        public static string ObjectToFormattedData(object data, Type dataType)
        {
            string str;

            switch (Type.GetTypeCode(dataType))
            {
                case TypeCode.Char:
                    str = data.ToString();
                    break;

                case TypeCode.Double:
                    str = ((double)data).ToString();
                    break;

                case TypeCode.Single:
                    str = ((float)data).ToString();
                    break;

                case TypeCode.UInt32:
                    str = ((UInt32)data).ToString();
                    break;

                case TypeCode.Int32:
                    str = ((Int32)data).ToString();
                    break;

                case TypeCode.Int16:
                    str = ((Int16)data).ToString();
                    break;

                case TypeCode.UInt16:
                    str = ((UInt16)data).ToString();
                    break;

                case TypeCode.SByte:
                    str = ((sbyte)data).ToString();
                    break;

                case TypeCode.Byte:
                    str = ((byte)data).ToString();
                    break;

                default:
                    str = data.ToString();
                    break;
            }

            return str;
        }
        */
        #endregion

        #region switch UnitSize
        private int UnitSize(Type type)
        {
            int unitSize;

            switch (Type.GetTypeCode(type))
            {
                // 16bit short signed and unsigned
                case TypeCode.Int16:
                case TypeCode.UInt16:
                    unitSize = 2;
                    break;

                // 32bit float || 32bit double || 32bit integer signed or unsigned or fourCC
                case TypeCode.String:
                case TypeCode.Double:
                case TypeCode.Single:
                case TypeCode.UInt32:
                case TypeCode.Int32:
                    unitSize = 4;
                    break;

                // char or byte signed and unsigned
                case TypeCode.Char:
                case TypeCode.SByte:
                case TypeCode.Byte:                  
                default:
                    unitSize = 1;
                    break;
            }

            return unitSize;
        }
        #endregion

        #region switch get type
        private Type GetType(UInt32 type)
        {
            Type ret;
            switch (type)
            {
                // char
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_UTC_DATE_TIME:
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_STRING_ASCII:
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_STRING_UTF8:
                    ret = typeof(char);
                    break;

                // float
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_FLOAT:
                    ret = typeof(float);
                    break;

                // double
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_DOUBLE:
                    ret =  typeof(double);
                    break;

                // complex
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_COMPLEX:
                    ret = typeof(object);
                    break;

                // fourCC
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_FOURCC:
                    ret = typeof(string);
                    break;

                // nest
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_NEST:
                    ret = null;
                    break;

                // single byte signed
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_SIGNED_BYTE:
                    ret = typeof(SByte);
                    break;

                // single byte unsigned
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_UNSIGNED_BYTE:
                    ret = typeof(Byte);
                    break;

                // 16bit integer signed
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_SIGNED_SHORT:
                    ret = typeof(Int16);
                    break;

                // 32bit integer signed
                case (UInt32)Common.GPMF_SAMPLETYPE.GPMF_TYPE_SIGNED_LONG:
                default:
                    ret = typeof(Int32);
                    break;
            }

            return ret;
        }
        #endregion
    }
}
